import random
import re
from datetime import datetime, timedelta
from xml.parsers.expat import ExpatError

import core
from pseudoclient.cmd_manager import *
from utils import *
from internets_utils import *
from api.feed import FeedError
from api.idlerpg import IrpgPlayer
from api.quotes import FmlException
from api.weather import WeatherException
from api.steam import SteamException

import pyva_net_rizon_acid_core_User as User

RE_YT_PATTERN = re.compile(
	"(?:www\\.|m\\.)?(?:(?:youtube\\.com/(?:watch)?(?:[?&][a-z]+=[a-z_]+)?(?:[?&]v=))"
	"|(?:youtu\\.be\\/))([a-zA-Z0-9-_]+)")


def onPrivmsg_regex_youtube(self, source, target, message):
	userinfo = User.findUser(source)
	myself = User.findUser(self.nick)

	sender = userinfo['nick']
	channel = target

	yt_links = RE_YT_PATTERN.findall(message)

	if yt_links and self.google._check_link_eligibility(channel, yt_links[0]):
		try:
			video = self.google.yt_video(yt_links[0], userip=userinfo['ip'] if userinfo['ip'] != '0' else None)
		except:
			# Silently die
			return
		self.msg(channel, "@sep @bYouTube@b %(title)s @sep (%(duration)s) @sep @bViews@b %(views)s @sep "\
				"@bRating@b @c3@b[+]@b %(liked)s likes @c4@b[-]@b %(disliked)s dislikes @sep" % {
				'title': video['title'],
				'duration': '%s' % format_hms(video['duration']),
				'views': format_thousand(video['view_count']),
				'liked': format_thousand(video['liked']),
				'disliked': format_thousand(video['disliked'])
				})
	else:
		return True


def get_citystate_from_zipcode(self, zipcode):
	"""Return [city,state] for the given U.S. zip code (if database has been imported)"""
	try:
		con = core.dbpool.get_connection()
		try:
			cursor = con.cursor()
			cursor.execute("SELECT city, state FROM zipcode_citystate WHERE zipcode=%s", [int(zipcode)])
			city, state = cursor.fetchone()
			return city, state
		finally:
			core.dbpool.put_connection(con)
	except:
		return None

##
# Returns colour coded test of persona state in Steam.
##
def get_personastate_text(self, state):
	if state == 0:
		# user is offline.
		return '@c14OFFLINE@c'
	elif state == 1:
		# user is online
		return '@c3ONLINE@c'
	elif state == 2:
		# user is busy
		return '@c4BUSY@c'
	elif state == 3:
		# user is away
		return '@c7AWAY@c'
	elif state == 4:
		# user is snooze
		return '@c7SNOOZE@c'
	elif state == 5:
		# user is looking to trade
		return '@c5LOOKING TO TRADE@c'
	elif state == 6:
		# user is looking to play
		return '@c10LOOKING TO PLAY@c'
	else:
		# unknown status
		return '@c14UNKNOWN@c'

def command_weather(self, manager, opts, arg, channel, sender, userinfo):
	arg = self.get_location(opts, arg, channel, sender)
	if not arg:
		return
	w_state = ''
	if arg.isdigit():
		location = get_citystate_from_zipcode(self, arg)
		if location is None:
			self.errormsg(channel, 'zip code not recognised.')
			return False
		city, state = location
		location = '{city}, {state}, USA'.format(city=city, state=state)
		w_state = state + u', '
	else:
		location = arg.strip()

	try:
		w = self.weather.get_conditions(location)
	except WeatherException as exc:
		if exc == 'this key is not valid':
			self.elog.warning('WARNING: OpenWeatherMap API key is not correctly set (%s)' % exc)
			self.errormsg(channel, 'weather data is temporarily unavailable. Try again later')
		else:
			self.errormsg(channel, exc)
		return
	except FeedError, e:
		self.errormsg(channel, e.msg)
		return

	code = get_tempcolor(w['temp_c'])

	self.msg(channel, format_weather(code, u"""@sep @b{w[city]}{w_state}{w[country]}@b @sep @bConditions@b {w[description]} @sep \
@bTemperature@b {tempcolor}{w[temp_c]}C / {w[temp_f]}F@o @sep \
@bPressure@b {w[pressure]}mb @sep @bHumidity@b {w[humidity]}% @sep \
@bRain@b {w[rain]} @sep \
Powered by OpenWeatherMap http://openweathermap.org/city/{w[id]} @sep""".format(w=w, tempcolor=code, w_state=w_state)))


def command_forecast(self, manager, opts, arg, channel, sender, userinfo):
	arg = self.get_location(opts, arg, channel, sender)
	if not arg:
		return
	w_state = ''
	if arg.isdigit():
		location = get_citystate_from_zipcode(self, arg)
		if location is None:
			self.errormsg(channel, 'zip code not recognised.')
			return False
		city, state = location
		location = '{city}, {state}, USA'.format(city=city, state=state)
		w_state = state + u', '
	else:
		location = arg.strip()

	try:
		w = self.weather.get_forecast(location)
	except WeatherException as exc:
		if exc == 'this key is not valid':
			self.elog.warning('WARNING: OpenWeatherMap API key is not correctly set (%s)' % exc)
			self.errormsg(channel, 'weather data is temporarily unavailable. Try again later')
		else:
			self.errormsg(channel, exc)
		return
	except FeedError, e:
		self.errormsg(channel, e.msg)
		return

	fc = ' @sep '.join([u"""@b{day[name]}@b {day[description]} @c04{day[max_c]}C / {day[max_f]}F \
@c10{day[min_c]}C / {day[min_f]}F""".format(day=day) for day in w['days']])

	self.msg(channel, u'@sep @b{w[city]}{w_state}{w[country]}@b @sep {fc} @sep'.format(w=w, fc=fc, w_state=w_state))


def command_register_location(self, manager, opts, arg, channel, sender, userinfo):
	arg = arg.strip()
	try:
		w_state = ''
		if arg.isdigit():
			location = get_citystate_from_zipcode(self, arg)
			if location is None:
				self.errormsg(channel, 'zip code not recognised.')
				return False
			city, state = location
			location = '{city}, {state}, USA'.format(city=city, state=state)
			w_state = state + u', '
		else:
			location = arg.strip()

		w = self.weather.get_conditions(location)
	except WeatherException as exc:
		if exc == 'this key is not valid':
			self.elog.warning('WARNING: OpenWeatherMap API key is not correctly set (%s)' % exc)
			self.errormsg(channel, 'weather data is temporarily unavailable. Try again later')
		else:
			self.errormsg(channel, exc)
		return
	except FeedError, e:
		self.errormsg(channel, e.msg)
		return

	loc_name = u'{w[city]}{w_state}{w[country]}'.format(w=w, w_state=w_state)

	self.users.set(sender, 'location', arg)
	self.msg(channel, u'%s: registered location @b%s@b' % (sender, loc_name))

def command_bing_translate(self, manager, opts, arg, channel, sender, userinfo):
	sp = arg.split(' ', 2)
	try:
		if len(sp) > 2:
			source, target, text = sp
			if source.lower() not in self.bing.languages or target.lower() not in self.bing.languages:
				source = self.bing.detect_language(arg)
				target = None
				translation = self.bing.translate(arg)
			else:
				source = source.lower()
				target = target.lower()
				translation = self.bing.translate(text, source, target)
		else:
			source = self.bing.detect_language(arg)
			target = None
			translation = self.bing.translate(arg)
	except FeedError, e:
		self.elog.warning('WARNING: Bing translate failed: %s' % e)
		self.errormsg(channel, e.msg)
		return
	
	self.msg(channel, '[t] [from %s] %s' % (source, translation))

def command_google_search(self, manager, opts, arg, channel, sender, userinfo):
	try:
		result = self.google.search(arg, userinfo['ip'] if userinfo['ip'] != '0' else None)
	except FeedError, e:
		self.errormsg(channel, e.msg)
		return
	
	if result['responseStatus'] == 403:
		self.elog.warning('WARNING: Google Search failed: %s' % result['responseDetails'] if 'responseDetails' in result else 'unknown error')
		self.notice(sender, 'Google Search is temporarily unavailable. Try again later.')
		return
	
	result = result['responseData']['results']
	if not result:
		self.msg(channel, '[Google] No results found')
		return
	
	json = result[0]
	self.msg(channel, '[Google] @b%(title)s@b <@u%(url)s@u>' % {
					'title': unescape(json['titleNoFormatting']),
					'url': json['unescapedUrl']})

	if json['content'] != '':
		self.msg(channel, '[Google] @bDescription@b: %s' % unescape(json['content']).replace('<b>', '@b').replace('</b>', '@b'))

def command_google_image_search(self, manager, opts, arg, channel, sender, userinfo):
	try:
		result = self.google.image_search(arg, userinfo['ip'] if userinfo['ip'] != '0' else None)
	except FeedError, e:
		self.errormsg(channel, e.msg)
		return
	
	if result['responseStatus'] == 403:
		self.elog.warning('WARNING: Google Image Search failed: %s' % result['responseDetails'] if 'responseDetails' in result else 'unknown error')
		self.notice(sender, 'Google Search is temporarily unavailable. Try again later.')
		return
	
	result = result['responseData']['results']
	if not result:
		self.msg(channel, '[Google Image] No results found')
		return
	
	json = result[0]
	self.msg(channel, '[Google Image] @b%(title)s@b <@u%(url)s@u>' % {
					'title': unescape(json['titleNoFormatting']),
					'width': json['width'],
					'height': json['height'],
					'url': json['unescapedUrl']})

	self.msg(channel, '[Google Image] @bSize@b: %(width)sx%(height)spx%(desc)s' % {

					'width': json['width'],
					'height': json['height'],
					'desc': (' - @bDescription@b: %s' % unescape(json['content']).replace('<b>', '@b').replace('</b>', '@b')) if json['content'] else ''})


def command_calc(self, manager, opts, arg, channel, sender, userinfo):
	try:  # local calculation using PyParsing
		result = self.nsp.eval(arg)
		self.msg(channel, '[calc] {} = {}'.format(arg, result))
	except:  # just throw it at W|A, hopefully they can get it
		try:
			result = self.wolfram.alpha(arg)
		except FeedError as e:
			self.errormsg(channel, e.msg)
			return

		if result is None:
			self.msg(channel, '[W|A] Invalid input.')
		else:
			resultLines = result[0].splitlines(True) if result[1] is None else result[1].splitlines(True)
			lines = len(resultLines) # number of lines
			maxLineLength = max([len(x) for x in resultLines]) # max length of lines

                        tosend = u'[W|A] {r[0]}'.format(r=result) # default to always show the first part
                        if result[1] is not None:
                            tosend += u' = {r[1]}'.format(r=result)

			if lines > self.output_limit or maxLineLength > self.max_line_length:
                                    self.notice(sender, tosend)
			else:
                                    self.msg(channel, tosend)

def command_youtube_search(self, manager, opts, arg, channel, sender, userinfo):
	try:
		res = self.google.yt_search(arg, userip=userinfo['ip'] if userinfo['ip'] != '0' else None)
	except FeedError, e:
		self.errormsg(channel, e.msg)
		return
	
	if not res:
		self.msg(channel, '[YouTube] No results found')
		return
	
	self.msg(channel, """@sep @bYouTube@b %(title)s @sep @bURL@b %(url)s (%(duration)s) @sep @bViews@b %(views)s @sep \
@bRating@b @c3@b[+]@b %(liked)s likes @c4@b[-]@b %(disliked)s dislikes @sep""" % {
			'title': res['title'],
			'url': 'http://www.youtube.com/watch?v=' + res['id'],
			'duration': '%s' % format_hms(res['duration']),
			'views': format_thousand(res['view_count']),
			'liked': format_thousand(res['liked']) if res['liked'] else 0,
			'disliked': format_thousand(res['disliked']) if res['disliked'] else 0
			})

def command_twitch(self, manager, opts, arg, channel, sender, userinfo):
	MAX_RESULTS = 5
	expr, sep, res_num = arg.partition('/')
	arg = expr.strip()
	try:
		if 'stream' in opts:
			if arg.strip(): # search for a particular stream
				[total, streams] = self.twitch.search_stream(arg)
				if not total:
					self.errormsg(channel, u"No results found.")
					return
				if res_num:
					s = streams[int(res_num)-1]
					self.msg(channel, (u"@sep Game @b{s[game]}@b @sep has status @b{s[status]}@b on channel @u{s[chan]}@u, " +
					u"which has {s[views]} views and {s[followers]} followers @sep Link: {s[url]} @sep").format(s=s))
					return
				i = 1
				for s in streams:
					if i > MAX_RESULTS: break
					self.msg(channel, (u"@sep [{i}/{total}] Game @b{s[game]}@b @sep has status @b{s[status]}@b on channel @u{s[chan]}@u, " +
					u"which has {s[views]} views and {s[followers]} followers @sep Link: {s[url]} @sep").format(i=i, total=total, s=s))
					i += 1
				self.notice(sender, "To view a particular result, type: @b.tw -s {} /@unumber@u@b".format(arg.replace('+', ' ')))
			else: # list top streams
				streams = self.twitch.get_streams()
				if res_num:
					s = streams[int(res_num)-1]
					self.msg(channel, (u"@sep Game @b{s[game]}@b @sep has a stream created on {s[created_at]} that has " +
					u"{s[viewers]} viewers @sep Related channel: @b{s[channel]}@b @sep").format(s=s))                             
					return
				i = 1
				total = len(streams)
				for s in streams:
					if i > MAX_RESULTS: break
					self.msg(channel, (u"@sep [{i}/{total}] Game @b{s[game]}@b @sep has a stream created on {s[created_at]} that has " +
					u"{s[viewers]} viewers @sep Related channel: @b{s[channel]}@b @sep").format(s=s, i=i, total=total))                                            
					i += 1
				self.notice(sender, u"To view a particular result, type: @b.tw -s /@unumber@u@b")

		elif 'channel' in opts:
			videos = self.twitch.get_channel(arg.strip())
			if videos:
				if res_num:
					v = videos[int(res_num)-1]
					self.msg(channel, u"@sep @b{v[title]}@b @sep with {v[views]} views @sep Link: {v[url]} @sep".format(v=v))
					return
				self.msg(channel, "@sep Videos in channel @b{}@b @sep".format(arg))
				i = 1
				total = len(videos)
				for v in videos:
					if i > MAX_RESULTS: break
					self.msg(channel, "@sep [{i}/{total}] @b{v[title]}@b @sep with {v[views]} views @sep Link: {v[url]} @sep"
						 .format(v=v, i=i, total=total))
					i += 1
				self.notice(sender, "To view a particular result, type: @b.tw -c {} /@unumber@u@b".format(arg.replace('+', ' ')))
			else:
				self.errormsg(channel, "Channel @b{}@b does not exit.".format(arg))

		elif 'game' in opts:
			games = self.twitch.search_games(arg)
			if not games:
				self.errormsg(channel, "Game @b{}@b does not exit.".format(arg))
				return
			if res_num:
				g = games[int(res_num)-1]
				self.msg(channel, (u"@sep @b{g[name]}@b @sep ranked @b{g[popularity]}@b in popularity @sep " +
						   u"http://www.twitch.tv/search?query={g[name2]} @sep").format(g=g))
				return
			self.msg(channel, "Results for @b{}@b".format(arg.replace('+', ' ')))
			i = 1
			total = len(games)
			for g in games:
				if i > MAX_RESULTS: break
				self.msg(channel, (u"@sep [{i}/{total}] @b{g[name]}@b @sep ranked @b{g[popularity]}@b in popularity @sep " +
					 u"http://www.twitch.tv/search?query={g[name2]} @sep").format(i=i, g=g, total=total))
				i += 1
			self.notice(sender, "To view a particular result, type: @b.tw -g {} /@unumber@u@b".format(arg.replace('+', ' ')))

		elif 'team' in opts:
			if arg.strip(): # search for a particular team
				t = self.twitch.get_team(arg)
				if t:
					self.msg(channel, (u"@sep Team @u{t[name]}@u @sep called @b{t[display_name]}@b @sep was created " +
					u"on {t[created_at]} @sep Info: {t[info]} @sep").format(t=t))
				else:
					self.errormsg(channel, "Team @b{}@b does not exist.".format(arg))
			else: # list top teams
				teams = self.twitch.get_teams()
				if not teams:
					self.errormsg(channel, u"No teams exist.")
					return
				if res_num:
					t = teams[int(res_num)-1]
					self.msg(channel, (u"@sep Team @u{t[name]}@u @sep called @b{t[display_name]}@b @sep was created " +
							   u"on {t[created_at]} @sep Info: {t[info]} @sep").format(t=t))
					return
				i = 1
				total = len(teams)
				for t in teams:
					if i > MAX_RESULTS: break
					self.msg(channel, (u"@sep [{i}/{total}] Team @u{t[name]}@u @sep called @b{t[display_name]}@b @sep was created " +
							   u"on {t[created_at]} @sep Info: {t[info]} @sep").format(t=t, i=i, total=total))
					i += 1
				self.notice(sender, u"To view a particular result, type: @b.tw -t /@unumber@u@b")
				
		elif 'video' in opts:
			self.msg(channel, "@sep http://www.twitch.tv/search?query={} @sep".format(arg.replace(' ', '+')))

		else:
			self.notice(sender, u"Type: @b.help twitch@b for the twitch.tv bot command syntax.")
	except ValueError:
		self.errormsg(channel, u"What's after the slash must be an integer number.")
	except IndexError:
		self.errormsg(channel, u"No result with that number exists.")
	

def command_dictionary(self, manager, opts, arg, channel, sender, userinfo):
	try:
		results = self.wordnik.definition(arg)
	except FeedError, e:
		self.errormsg(channel, e.msg)
		return
	
	if not results:
		self.msg(channel, '[dictionary] Nothing found')
		return
	
	if 'all' in opts:
		for n, res in enumerate(results, 1):
			self.notice(sender, u'@sep [{num}/{tot}] @bDefinition@b {res.word} @sep {res.text} @sep'.format(
						res=res, num=n, tot=len(results)))
	elif 'number' in opts:
		if opts['number'] - 1 < 0 or opts['number'] - 1 > len(results):
			self.errormsg(channel, 'option -n out of range: only %d definitions found.' % len(results))
			return
		
		result = results[opts['number'] - 1]
		self.msg(channel, u'@sep [{num}/{tot}] @bDefinition@b {res.word} @sep {res.text} @sep'.format(
						res=result, num=opts['number'], tot=len(results)))
	else:
		for n, res in enumerate(results, 1):
			self.msg(channel, u'@sep [{num}/{tot}] @bDefinition@b {res.word} @sep {res.text} @sep'.format(
					res=res, num=n, tot=len(results)))
			if n == 4:
				self.notice(sender, 'To view all definitions: .dict {res.word} -a. To view the n-th definition: .dict {res.word} -n <number>'.format(res=res))
				break

def command_urbandictionary(self, manager, opts, arg, channel, sender, userinfo):
	expr, sep, def_id = arg.partition('/')
	try:
		res = self.urbandictionary.get_definitions(expr.strip())
	except FeedError, e:
		self.errormsg(channel, e.msg)
		self.elog.warning('feed error in .urbandictionary: %s' % e)
		return
	
	if 'list' not in res or len(res['list']) < 1:
		self.errormsg(channel, 'no results found')
	else:
		if def_id:
			try:
				def_id = int(def_id)
				if def_id < 1:
					self.errormsg(channel, 'invalid definition number')
					return
				
				entry = res['list'][def_id - 1]
				definition = entry['definition'].replace('\r\n', ' / ').replace('\n', ' / ')
				example = entry['example'].replace('\r\n', ' / ').replace('\n', ' / ')
				self.msg(channel, u'@sep [{num}/{total}] {entry[word]} @sep {definition} @sep'.format(
						num = def_id,
						total = len(res['list']),
						res = res,
						definition = definition if len(definition) < 200 else definition[:200] + '...',
						entry = entry))
				self.msg(channel, u'@sep @bExample@b %s @sep' % (example if len(example) < 280 else example[:280] + '...'))
			except ValueError:
				self.errormsg(channel, 'invalid definition number')
			except IndexError:
				self.errormsg(channel, 'definition id out of range: only %d definitions available' % len(res['list']))
		else:
			for num, entry in enumerate(res['list'], 1):
				if num == 4:
					self.notice(sender, u'To view a single definition with a related example, type: @b.u %s /def_number@b. For more definitions, visit: %s' % (expr, res['list'][0]['permalink']))
					break
				
				definition = entry['definition'].replace('\r\n', ' / ').replace('\n', ' / ')
				self.msg(channel, u'@sep [{num}/{total}] {entry[word]} @sep {definition} @sep'.format(
						num = num,
						total = len(res['list']),
						res = res,
						definition = definition if len(definition) < 200 else definition[:200] + '...',
						entry = entry))

def command_imdb(self, manager, opts, arg, channel, sender, userinfo):
	try:
		reply = self.imdb.get(arg)
	except FeedError, e:
		self.errormsg(channel, e.msg)
		return
	except ValueError:
		self.errormsg(channel, 'movie not found')
		return
	
	if reply['Response'] != 'True':
		self.msg(channel, '[imdb] Nothing found')
		return
	
	self.msg(channel, u"""@sep @b{r[Title]}@b [{r[Year]}] Rated {r[Rated]} @sep @bRating@b {r[imdbRating]}/10, {r[imdbVotes]} votes @sep \
@bGenre@b {r[Genre]} @sep @bDirector@b {r[Director]} @sep @bActors@b {r[Actors]} @sep @bRuntime@b {r[Runtime]} @sep""".format(r=reply))
	self.msg(channel, u'@sep @bPlot@b {r[Plot]} @sep @uhttp://www.imdb.com/title/{r[imdbID]}/@u @sep'.format(r=reply))

#
# Registers the user's steam ID and links it to his/her nickname.
#
def command_register_steam(self, manager, opts, arg, channel, sender, userinfo):
	arg = arg.strip()
	try:		
		steam_data = self.steam.find_user(arg)
		self.notice(sender, u'Steam ID registered, current personaname: {name}'.format(name = steam_data['personaname']))
		self.users.set(sender, 'steamid', steam_data['steamid'])
	except SteamException as exc:
		self.notice(sender, 'No user found')		

##
# Shows user's online status and what game he/sh is playing.
# Game does not show when user's profile is set to private.
##
def command_steam(self, manager, opts, arg, channel, sender, userinfo):
	steamuser = self.get_steamid(opts, arg, channel, sender)
	if not steamuser:
		return
	steam_data = self.steam.get_status(steamuser.steamid)

	if steam_data['communityvisibilitystate'] == 1:
		# Profile is hidden
		self.notice(sender, 'Profile is hidden. If you want to use this functionality, set your profile to Public.')
		return

	if 'games' in opts:
		steam_games = self.steam.get_games(steamuser.steamid)		
		playtime_forever = 0
		playtime_2weeks = 0
		playtime_forever_top = 0
		game_forever = ""
		playtime_2weeks_top = 0
		game_2weeks = ""
		message = u"""@sep @b{player}@b @sep""".format(player = steam_data['personaname'])

		if steam_games['game_count'] == 0:
			# You know, because it's possible
			message += u""" Does not own any games. @sep"""
			self.msg(channel, message)
			return

		for item in steam_games['games']:			
			ptf = item['playtime_forever']			
			if ptf > playtime_forever_top:
				game_forever = item['appid']
				playtime_forever_top = ptf
			playtime_forever += ptf

			try:
				ptw = item['playtime_2weeks']
				if ptw > playtime_2weeks_top:
					game_2weeks = item['appid']
					playtime_2weeks_top = ptw
				playtime_2weeks += ptw
			except Exception:
				#just skip it
				continue

		message += u""" @bTotal games:@b {total} @sep @bTotal playtime:@b {ftime} hours @sep @bPlaytime last 2 weeks:@b {wtime} hours @sep""".format(
			total = steam_games['game_count'],
			ftime = round(playtime_forever / 60, 0),
			wtime = round(playtime_2weeks / 60, 0))
		if game_forever != "":
			fgame = self.steam.get_game_name(game_forever)
			message += u""" @bMost played game:@b {fgame}, {ftime} hours @sep""".format(
				fgame = fgame,
				ftime = round(playtime_forever_top / 60, 0))
		if game_2weeks != "":
			wgame = self.steam.get_game_name(game_2weeks)
			message += u""" @bMost played last 2 weeks:@b {wgame}, {wtime} hours @sep""".format(
				wgame = wgame,
				wtime = round(playtime_2weeks / 60, 0))
		self.msg(channel, message)
	else:
		# Prepare message
		message = u"""@sep @b{player}@b [{status}] @sep""".format(
			player = steam_data['personaname'],
			status = get_personastate_text(self, steam_data['personastate']))
		if steam_data['personastate'] == 0 or steam_data['personastate'] > 7:
			# User is offline or unknown state
			# NOTE: lastlogoff is actual logoff timestamp, not "appear offline" timestamp
			latestdate = get_timespan(datetime.fromtimestamp(steam_data['lastlogoff']))
			message += u""" @bLast seen@b {latestdate} ago @sep""".format(
				latestdate = latestdate)
			self.msg(channel, message)
		else:
			# user is online, busy, away, snooze, looking to trade or looking to play
			if 'gameid' in steam_data:
				message += u""" @bPlaying:@b {gamename} @sep""".format(
					gamename = self.steam.get_game_name(steam_data['gameid']))
				if 'gameserverip' in steam_data:
					message += u""" @bPlaying on server:@b {gameserver} @sep""".format(
					gameserver = steam_data['gameserverip'])
			else:
				# User is not playing a game.
				message += u""" Not playing anything right now @sep"""

			self.msg(channel, message)

def command_lastfm(self, manager, opts, arg, channel, sender, userinfo):
	try:
		user = self.lastfm.get_user(arg)
		if 'error' in user:
			self.errormsg(channel, user['message'])
			return
		latest = self.lastfm.get_recent_tracks(arg, 1)
	except FeedError as e:
		self.errormsg(channel, e.msg)
		return
	
	user = user['user']
	userinfo = []
	if 'realname' in user:
		if user['realname']:
			userinfo.append(user['realname'])
	else:
		userinfo.append(user['name'])
	if 'age' in user:
		if user['age']:
			userinfo.append(user['age'])
	if 'country' in user:
		if user['country']:
			userinfo.append(user['country'])

	userinfo = ' [%s]' % ', '.join(userinfo)

	if 'track' in latest['recenttracks']:
		if isinstance(latest['recenttracks']['track'], list):
			if latest['recenttracks']['track']:
				latest = latest['recenttracks']['track'][0]
			else:
				lastest = ''
		else:
			latest = latest['recenttracks']['track']
		try:
			latest['@attr']['nowplaying']
			latest_str = u' @bNow playing@b {latest[artist][#text]} - {latest[name]} @sep'.format(latest=latest)
		except KeyError:
			if 'date' in latest:
				latestdate = get_timespan(datetime.fromtimestamp(int(latest['date']['uts'])))
				latest_str = u' @bLatest track@b {latest[artist][#text]} - {latest[name]} ({latestdate} ago) @sep'.format(
					latest=latest, latestdate=latestdate)
			else:
				latest_str = ''
	else:
		latest_str = ''
	
	regdate = datetime.fromtimestamp(int(user['registered']['unixtime'])).strftime('%Y-%m-%d %H:%M:%S')

	self.msg(channel, u'@sep @b{user[name]}@b{userinfo} @sep @bPlays@b {plays} since {regdate} @sep \
@bLink@b {user[url]} @sep{latest_track}'.format(
				userinfo = userinfo,
				plays = format_thousand(int(user['playcount'])),
				regdate = regdate,
				user = user,
				latest_track = latest_str))

def command_url_shorten(self, manager, opts, arg, channel, sender, userinfo):
	if not arg.startswith('http://') and not arg.startswith('https://'):
		self.errormsg(channel, 'a valid URL must start with http:// or https://')
		return
	
	try:
		reply = self.urls.shorten(arg)
	except FeedError, e:
		self.errormsg(channel, e.msg)
		return
	
	if reply['status_code'] != 200:
		self.errormsg(channel, 'an error occurred.')
		self.elog.warning('[shorten] error: code %d, %s' % (reply['status_code'], reply['status_txt']))
	else:
		self.msg(channel, '@sep @bShort URL@b %s @sep' % reply['data']['url'])

def command_url_expand(self, manager, opts, arg, channel, sender, userinfo):
	if not arg.startswith('http://') and not arg.startswith('https://'):
		self.errormsg(channel, 'a valid URL must start with http:// or https://')
		return
	
	try:
		reply = self.urls.expand(arg)
	except FeedError, e:
		self.errormsg(channel, e.msg)
		return
	
	if 'error' in reply:
		self.errormsg(channel, reply['error'])
	elif not reply['long-url']:
		self.msg(channel, '@sep @bLong URL@b URL does not redirect')
	else:
		self.msg(channel, '@sep @bLong URL@b {reply[long-url]} @sep'.format(reply=reply))

def command_idlerpg(self, manager, opts, arg, channel, sender, userinfo):
	try:
		player = IrpgPlayer(arg)
	except FeedError, e:
		self.errormsg(channel, e.msg)
		return
	
	if not player.name:
		self.errormsg(channel, 'player not found. @bNote@b: nicks are case sensitive.')
		return
	
	self.msg(channel, """@sep @b{player.name}@b [{status}] @sep @bLevel@b {player.level} {player.classe} @sep @bNext level@b \
{nextlevel} @sep @bIdled@b {idled_for} @sep @bAlignment@b {player.alignment} @sep""".format(
				player = player,
				status = '@c3ON@c' if player.is_online else '@c4OFF@c',
				nextlevel = timedelta(seconds=player.ttl),
				idled_for = timedelta(seconds=player.idled_for)))

def command_ipinfo(self, manager, opts, arg, channel, sender, userinfo):
	try:
		reply = self.ipinfo.get_info(arg)
	except FeedError, e:
		self.errormsg(channel, e.msg)
		return
	
	self.msg(channel, """@sep @bIP/Host@b {arg} ({reply[ip_addr]}) @sep @bLocation@b {reply[city]}, {reply[region]}, \
{reply[country_name]} [{reply[country_code]}] @sep{map}""".format(
				reply = reply,
				arg = arg.lower(),
				map = ' http://maps.google.com/maps?q=%s,%s @sep' % (reply['latitude'], reply['longitude']) if reply['latitude'] and reply['longitude'] else ''))
	
dice_regex = re.compile('^(?:(\d+)d)?(\d+)(?:([\+\-])(\d+))?$')

def command_dice(self, manager, opts, arg, channel, sender, userinfo):
	r = dice_regex.search(arg)
	if not r:
		self.errormsg(channel, 'invalid format')
		return
	
	num, faces, type, modifier = r.groups()
	if num:
		num = int(num)
	else:
		num = 1
	faces = int(faces)
	if num < 1 or num > 32 or faces < 2 or faces > 65536:
		self.errormsg(channel, 'parameter out of range')
		return
	
	total = 0
	results = []
	for n in xrange(int(num)):
		randnum = random.randint(1, int(faces))
		total += randnum
		results.append(randnum)
	
	if type == '-':
		modifier = int(modifier)
		total -= modifier
		max = num * faces - modifier
	elif type == '+':
		modifier = int(modifier)
		total += modifier
		max = num * faces + modifier
	else:
		max = num * faces
		
	self.msg(channel, '@sep @bTotal@b {total} / {max} [{percent}%] @sep @bResults@b {results} @sep'.format(
				total = total,
				max = max,
				percent = 100 * total / max if max != 0 else '9001',
				results = str(results)))

def command_qdb(self, manager, opts, arg, channel, sender, userinfo):
	try:
		if not arg:
			quote = self.quotes.get_qdb_random()
		else:
			try:
				quote_id = int(arg)
				quote = self.quotes.get_qdb_id(quote_id)
				if not quote:
					self.errormsg(channel, 'quote @b%d@b not found' % quote_id)
					return
			except ValueError:
				self.errormsg(channel, 'invalid quote ID')
				return
			except ExpatError: # qdb returns a malformed xml when the quote doesn't exist
				self.errormsg(channel, 'quote @b%d@b not found' % quote_id)
				return
	except FeedError, e:
		self.errormsg(channel, e.msg)
		return
	
	id = quote['id']
	for line in quote['lines']:
		self.msg(channel, u'[qdb {id}] {line}'.format(id=id, line=line.replace('\n', '')))

def command_fml(self, manager, opts, arg, channel, sender, userinfo):
	try:
		if not arg:
			quote = self.quotes.get_fml()
		else:
			try:
				quote_id = int(arg)
				quote = self.quotes.get_fml(quote_id)
				if not quote:
					self.errormsg(channel, 'quote @b%d@b not found' % quote_id)
					return
			except (ValueError, IndexError):
				self.errormsg(channel, 'invalid quote ID')
				return
	except (FeedError, FmlException) as e:
		self.errormsg(channel, e.msg)
		self.elog.warning('WARNING: .fml error: %s' % e.msg)
		return

	self.msg(channel, u'[fml #{quote[id]}] {quote[text]}'.format(quote=quote))

def command_internets_info(self, manager, opts, arg, channel, sender, userinfo):
	self.notice(sender, '@sep @bRizon Internets Bot@b @sep @bDevelopers@b martin <martin@rizon.net> @sep @bHelp/feedback@b %(channel)s @sep' % {
		'channel' : '#internets'})

def command_internets_help(self, manager, opts, arg, channel, sender, userinfo):
	command = arg.lower()

	if command == '':
		message = ['internets: .help internets - for internets commands']
	elif command == 'internets':
		message = manager.get_help()
	else:
		message = manager.get_help(command)

		if message == None:
			message = ['%s is not a valid command.' % arg]

	for line in message:
		self.notice(sender, line)

class UserCommandManager(CommandManager):
	def get_prefix(self):
		return '.'

	def get_commands(self):
		return {
			'cc': 'calc',
			'calc': (command_calc, ARG_YES, 'Calculates an expression', [], 'expression'),
			
			'dict': 'dictionary',
			'dictionary': (command_dictionary, ARG_YES, 'Search for a dictionary definition', [
				('number', '-n', 'display the n-th result', {'type': '+integer'}, ARG_YES),
				('all', '-a', 'display all results (using /notice)', {'action': 'store_true'}, ARG_YES)], 'word'),
			
			'u': 'urbandictionary',
			'urbandictionary': (command_urbandictionary, ARG_YES, 'Search for a definition on Urban Dictionary', [], 'word'),
			
			# 'g': 'google',
			# 'google': (command_google_search, ARG_YES, 'Search for something on Google', [], 'google_search'),
			
			# 'gi': 'google_image',
			# 'google_image': (command_google_image_search, ARG_YES, 'Search for images via Google Image', [], 'google_image_search'),

			't': 'translate',
			'translate': (command_bing_translate, ARG_YES, 'Translate something from a language to another', [], 'from to text'),
			
			'yt': 'youtube',
			'youtube': (command_youtube_search, ARG_YES, 'Search for something on YouTube', [], 'youtube_search'),
			
			'w': 'weather',
			'weather': (command_weather, ARG_OPT, 'Displays current weather conditions for a location', [
				('nick', '-n', 'use the weather location linked to a nick', {'action': 'store_true'}, ARG_YES)]),
			
			'f': 'forecast',
			'forecast': (command_forecast, ARG_OPT, 'Displays 5-day forecast for a location', [
				('nick', '-n', 'use the weather location linked to a nick', {'action': 'store_true'}, ARG_YES)]),
			
			'regloc': 'register_location',
			'register_location': (command_register_location, ARG_YES, 'Links a location to your nick that will be used as default location in .w and .f', [], 'location'),
			
			'imdb': (command_imdb, ARG_YES, 'Search for information on a movie on IMDB', [], 'movie_title'),
			
			'lastfm': (command_lastfm, ARG_YES, 'Returns information on a Last.fm user', [], 'lastfm_user'),
			
			'shorten': (command_url_shorten, ARG_YES, 'Shortens a URL using http://j.mp', [], 'long_url'),
			
			'unshorten': 'expand',
			'expand': (command_url_expand, ARG_YES, 'Expands a shortened URL using http://longurl.org', [], 'shortened_url'),
			
			'irpg': 'idlerpg',
			'idlerpg': (command_idlerpg, ARG_YES, 'Returns info on a player in Rizon IdleRPG (http://idlerpg.rizon.net/)', [], 'player_name'),
			
			'ipinfo': (command_ipinfo, ARG_YES, 'Returns short info on a IP address/hostname', [], 'ip/host'),
			
			'd': 'dice',
			'dice': (command_dice, ARG_YES, 'Rolls X N-sided dice with an optional modifier A (XdN+A format)', [], 'dice_notation'),
			
			'qdb': (command_qdb, ARG_OPT, 'Displays a quote from qdb.us', []),
			
			'fml': (command_fml, ARG_OPT, 'Displays a quote from http://www.fmylife.com', []),
			
			'steam': (command_steam, ARG_OPT, 'Shows your steam information', [
				('nick', '-n', 'use the steamid linked to a nick.', {'action': 'store_true'}, ARG_YES),
				('games', '-g', 'shows the total games owned by nick and shows most played game.', {'action': 'store_true'}, ARG_NO)]),
			
			'regsteam': 'register_steam',
			'register_steam': (command_register_steam, ARG_YES, 'Registers your Steam user ID', [], 'steamid'),

			'tw': 'twitch',
			'twitch': (command_twitch, ARG_OPT, 'Displays information about twitch.tv streams/games/channels/teams', [
				('stream', '-s', 'searches for a particular stream, or displays the top streams if no argument provided', {'action': 'store_true'}, ARG_OPT),
				('channel', '-c', 'lists the top videos of a particular channel on twitch.tv', {'action': 'store_true'}, ARG_YES),
				('game', '-g', 'searches for a game on twitch.tv', {'action': 'store_true'}, ARG_YES),
				('video', '-v', 'allows you to search the twitch.tv site for a video', {'action': 'store_true'}, ARG_YES),
				('team', '-t', 'displays the info of a particular stream on twitch.tv, or or displays the top teams if no argument provided', {'action': 'store_true'}, ARG_OPT)]),
			
			'info': (command_internets_info, ARG_NO|ARG_OFFLINE, 'Displays version and author information', []),
			'help': (command_internets_help, ARG_OPT|ARG_OFFLINE, 'Displays available commands and their usage', []),
		}

class Article:
	def __init__(self, a):
		header = a.getpath(a.xpath("//a[@class='articleTitle']")[0])[:-3]

		self.title = a.xpath(header + "/a[1]//text()")
		self.writer = a.xpath(header + "/a[2]//text()")
		self.votes = a.xpath(header + "/div")[1].text.strip()
		self.subs = a.xpath("//div[@class='bigArticleTab']")[1].text.strip()

from api import utils
from copy import copy
from pseudoclient.argparser import *


def check_rank(option, opt, value):
	try:
		return utils.get_rank(value)
	except:
		raise OptionValueError('option %s: invalid rank: %s' % (opt, value))

def check_military_unit_bonus(option, opt, value):
	try:
		i = int(value)
	except ValueError:
		raise OptionValueError('option %s: invalid integer value: %s' % (opt, value))
	
	if i not in [5, 10, 15, 20]:
		raise OptionValueError('option %s: invalid MU bonus percentage: %s' % (opt, value))
	else:
		return i

def check_defense_system_bonus(option, opt, value):
	try:
		i = int(value)
	except ValueError:
		raise OptionValueError('option %s: invalid integer value: %s' % (opt, value))

	if i < 1 or i > 5:
		raise OptionValueError('option %s: invalid defense system level: %s' % (opt, value))
	else:
		return i


class ESimParserOption(ArgumentParserOption):
	TYPES = ArgumentParserOption.TYPES + ('rank', 'mu_bonus', 'ds_bonus')
	TYPE_CHECKER = copy(ArgumentParserOption.TYPE_CHECKER)
	TYPE_CHECKER['rank']     = check_rank
	TYPE_CHECKER['mu_bonus'] = check_military_unit_bonus
	TYPE_CHECKER['ds_bonus'] = check_defense_system_bonus

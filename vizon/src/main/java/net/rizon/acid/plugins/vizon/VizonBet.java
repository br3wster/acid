/*
 * Copyright (c) 2017, orillion <orillion@rizon.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package net.rizon.acid.plugins.vizon;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;

/**
 *
 * @author orillion <orillion@rizon.net>
 */
public class VizonBet
{
	private final int id;
	private final int userId;
	private final int drawingId;
	private final Bet bet;
	private final LocalDateTime placed;

	public static VizonBet fromResultSet(ResultSet rs)
	{
		try
		{
			int id = rs.getInt("id");
			int userId = rs.getInt("vizon_users_id");
			int drawingId = rs.getInt("vizon_drawings_id");

			int first = rs.getInt("first");
			int second = rs.getInt("second");
			int third = rs.getInt("third");
			int fourth = rs.getInt("fourth");
			int fifth = rs.getInt("fifth");
			int sixth = rs.getInt("sixth");

			Bet bet = new Bet(first, second, third, fourth, fifth, sixth);

			Timestamp ts = rs.getTimestamp("placed");

			return new VizonBet(id, userId, drawingId, bet, ts.toLocalDateTime());
		}
		catch (SQLException ex)
		{
			return null;
		}
	}

	private VizonBet(int id, int userId, int drawingId, Bet bet, LocalDateTime placed)
	{
		this.id = id;
		this.userId = userId;
		this.drawingId = drawingId;
		this.bet = bet;
		this.placed = placed;
	}

	public int getId()
	{
		return id;
	}

	public int getUserId()
	{
		return userId;
	}

	public int getDrawingId()
	{
		return drawingId;
	}

	public Bet getBet()
	{
		return bet;
	}

	public LocalDateTime getPlaced()
	{
		return placed;
	}
}

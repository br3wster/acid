/*
 * Copyright (c) 2017, Orillion <orillion@rizon.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package net.rizon.acid.plugins.vizon.commands;

import java.util.regex.Matcher;
import net.rizon.acid.core.AcidUser;
import net.rizon.acid.core.Acidictive;
import net.rizon.acid.core.Channel;
import net.rizon.acid.core.Command;
import net.rizon.acid.core.User;
import net.rizon.acid.plugins.vizon.VhostManager;
import net.rizon.acid.plugins.vizon.Vizon;
import net.rizon.acid.plugins.vizon.db.VizonDatabase;
import net.rizon.acid.plugins.vizon.db.VizonUser;

/**
 *
 * @author Orillion <orillion@rizon.net>
 */
public class ModifyVhostCommand extends Command
{

	private static final String UPDATE_MESSAGE_FORMAT = "%s updated the vhost of %s to %s";

	public ModifyVhostCommand()
	{
		super(3, 3);
	}

	@Override
	public void Run(User source, AcidUser to, Channel c, String[] args)
	{
		String command = args[0];
		String nick = args[1];
		String vhost = args[2];

		if ("SET".equalsIgnoreCase(command))
		{
			this.set(source, to, c, nick, vhost);
		}
	}

	private void set(User source, AcidUser to, Channel c, String target, String vhost)
	{
		VizonDatabase database = Vizon.getVizonDatabase();

		VizonUser vizonUser = database.findUser(target);

		if (vizonUser == null)
		{
			Acidictive.reply(source, to, c, "Nickname not in database");
			return;
		}

		if (vizonUser.getObtained() == null)
		{
			Acidictive.reply(source, to, c, "User has no VIP status");
			return;
		}

		if (vizonUser.getVhost() == null)
		{
			// @NOTE(Orillion): Do not set when user does not have one yet,
			// this can conflict with HostServ, and I don't want to do extra
			// work here
			Acidictive.reply(source, to, c, "User does not have a vhost yet");
			return;
		}

		Matcher matcher;

		if (vizonUser.isBold())
		{
			matcher = VhostManager.BOLD_PATTERN.matcher(vhost);
		}
		else
		{
			matcher = VhostManager.NORMAL_PATTERN.matcher(vhost);
		}

		if (!matcher.matches())
		{
			// Vhost contains illegal characters
			Acidictive.reply(source, to, null, "Requested vhost contains illegal characters, or is empty");
			return;
		}

		if (vhost.getBytes().length > VhostManager.MAX_HOST_BYTES)
		{
			// We count bytes instead of chars, since counting chars is wrong.
			Acidictive.reply(source, to, null, "Requested vhost is too long");
			return;
		}

		if (vhost.charAt(vhost.length() - 1) != '\u000F')
		{
			// Add mandatory reset all character
			vhost = vhost + '\u000F';
		}

		vizonUser.setVhost(vhost);

		database.updateUser(vizonUser);

		User user = User.findUser(target);

		if (user != null)
		{
			Vizon.getVhostManager().applyVhostIfApplicable(user);
		}

		Acidictive.reply(source, to, c, String.format(
				"Updated the vhost of %s to %s",
				vizonUser.getNick(),
				vizonUser.getVhost()));

		Channel channel = Channel.findChannel(Acidictive.conf.getChannelNamed("vhost"));

		// Report manual update to vhost channel.
		Acidictive.reply(
				null,
				Vizon.getVizonBot(),
				channel,
				String.format(
						UPDATE_MESSAGE_FORMAT,
						source.getNick(),
						vizonUser.getNick(),
						vizonUser.getVhost()));
	}

	@Override
	public void onHelp(User u, AcidUser to, Channel c)
	{
		Acidictive.reply(u, to, c, "\002VHOST SET \u001Fnick\u001F \u001Fvhost\u001F\002 Change user's vhost");
	}

	@Override
	public boolean onHelpCommand(User u, AcidUser to, Channel c)
	{
		Acidictive.reply(u, to, c, "Syntax: \002VHOST SET \u001Fnick\u001F \u001Fvhost\u001F\002 Change user's vhost");
		Acidictive.reply(u, to, c, " ");
		Acidictive.reply(u, to, c, "Examples:");
		Acidictive.reply(u, to, c, "    \002VHOST SET magic \00313rainbows.are.magic\003\002");
		Acidictive.reply(u, to, c, "        Sets magic's colored vhost to \00313rainsbow.are.magic\003");

		return true;
	}
}

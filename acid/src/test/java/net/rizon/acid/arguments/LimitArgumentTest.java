package net.rizon.acid.arguments;

import org.junit.Assert;
import org.junit.Test;

/**
 * Unit tests for the LimitArgument class, what else did you expect?
 *
 * @author orillion <orillion@rizon.net>
 */
public class LimitArgumentTest
{
	private static final String MAX_INT_PLUS_ONE_LIMIT = "+2147483648";
	private static final String MAX_INT_LIMIT = "+2147483647";

	/**
	 * Tests whether or not a positive limit of 1 returns a limit of 1.
	 */
	@Test
	public void positiveOneTest()
	{
		int iExpected = 1;
		String sExpected = "+1";

		LimitArgument argument = LimitArgument.parse(sExpected);

		int iActual = argument.getLimit();
		String sActual = argument.getArgument();

		Assert.assertEquals(iExpected, iActual);
		Assert.assertEquals(sExpected, sActual);
	}

	/**
	 * Tests whether or not a negative limit returns null, instead of a negative
	 * limit, which is not what we want.
	 */
	@Test
	public void negativeOneTest()
	{
		String arg = "+-1";

		LimitArgument argument = LimitArgument.parse(arg);

		Assert.assertNull(argument);
	}

	/**
	 * Tests whether or not 0 returns null, instead of a limit of 0, which is
	 * not what we want.
	 */
	@Test
	public void positiveZeroTest()
	{
		String arg = "+0";

		LimitArgument argument = LimitArgument.parse(arg);

		Assert.assertNull(argument);
	}

	/**
	 * Tests whether or not this returns the correct value.
	 */
	@Test
	public void positiveMaxIntTest()
	{
		int iExpected = Integer.MAX_VALUE;
		String sExpected = MAX_INT_LIMIT;

		LimitArgument argument = LimitArgument.parse(sExpected);

		int iActual = argument.getLimit();
		String sActual = argument.getArgument();

		Assert.assertEquals(iExpected, iActual);
		Assert.assertEquals(sExpected, sActual);
	}

	/**
	 * Tests whether or not this returns null, since it is out of bounds.
	 */
	@Test
	public void positiveMaxIntPlusOneTest()
	{
		String arg = MAX_INT_PLUS_ONE_LIMIT;

		LimitArgument argument = LimitArgument.parse(arg);

		Assert.assertNull(argument);
	}

	/**
	 * Tests whether or not this returns the correct value.
	 */
	@Test
	public void positiveCorrect01Test()
	{
		int iExpected = 42;
		String sExpected = "+42";

		LimitArgument argument = LimitArgument.parse(sExpected);

		int iActual = argument.getLimit();
		String sActual = argument.getArgument();

		Assert.assertEquals(iExpected, iActual);
		Assert.assertEquals(sExpected, sActual);
	}

	/**
	 * Tests whether or not this returns null, when the argument is not prefixed
	 * by a + character.
	 */
	@Test
	public void NoPlusTest()
	{
		String arg = "12";

		LimitArgument argument = LimitArgument.parse(arg);

		Assert.assertNull(argument);
	}

	/**
	 * Tests whether this returns null when you enter null.
	 */
	@Test
	public void NullTest()
	{
		String arg = null;

		LimitArgument argument = LimitArgument.parse(arg);

		Assert.assertNull(argument);
	}
}

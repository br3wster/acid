package net.rizon.acid.commands;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import net.rizon.acid.core.AcidCore;
import net.rizon.acid.core.AcidUser;
import net.rizon.acid.core.Acidictive;
import static net.rizon.acid.core.Acidictive.reply;
import net.rizon.acid.core.Channel;
import net.rizon.acid.core.Command;
import net.rizon.acid.core.Message;
import net.rizon.acid.core.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Handles access to acid4 and its plugins.
 */
public class Access extends Command
{
	private static final Logger log = LoggerFactory.getLogger(Access.class);

	public Access()
	{
		super(1, 4);
	}

	private static boolean isSRA(final String flags)
	{
		return (flags.indexOf('1') != -1
						|| flags.contains("SRA")
						|| flags.indexOf('X') != -1);
	}

	private static void updateFlags(final String user, final String flags)
	{
		User u = User.findUser(user);
		if (u != null)
			u.setFlags(flags);
	}

	private static boolean isValidCertFP(final String certfp)
	{
		if (certfp.length() != 40)
			return false;

		for (char c : certfp.toLowerCase().toCharArray())
			/* Not using the Character.* functions because we don't need weird
			 * locales making weird characters "legal"
			 */
			if (c < '0' || c > '9' && (c < 'a' || c > 'f'))
				return false;

		return true;
	}

	/**
	 * Adds a certfp to the access entry. This function will add a certfp and
	 * notice the user about it. No effect if the target user has no certfp.
	 * @param u the user to check
	 * @param notice send a notice to the user that the certfp was added?
	 * @return true if a certfp was added, false otherwise
	 */
	public static boolean addCertFP(User u, boolean notice)
	{
		if (u.getCertFP().isEmpty())
			return false;
		if (u.getSU().isEmpty()) // redundant with Acidictive; not with access add!
			return false;

		try
		{
			PreparedStatement ps = Acidictive.acidcore_sql.prepare("SELECT `certfp` FROM `access` WHERE `user` = ?");
			ps.setString(1, u.getSU());
			ResultSet rs = Acidictive.acidcore_sql.executeQuery(ps);
			if (rs.next() && rs.getString("certfp").isEmpty())
			{
				ps = Acidictive.acidcore_sql.prepare("UPDATE `access` SET `certfp` = ? WHERE `user` = ?");
				ps.setString(1, u.getCertFP());
				ps.setString(2, u.getSU());
				ps.executeUpdate();

				if (notice)
					Acidictive.notice(u, "CertFP " + u.getCertFP() + " has been added to your access list.");
			}
			else
			{
				return false;
			}
		}
		catch (SQLException ex)
		{
			log.warn(null, ex);
		}

		return true;
	}

	@Override
	public void Run(User x, AcidUser to, Channel c, final String[] args)
	{
		// changecertfp
		if (x.getIdentNick().length() > 0 && args[0].equalsIgnoreCase("changecertfp"))
		{
			if (args.length < 2)
			{
				reply(x, to, c, "Invalid arguments: access changecertfp <certfp>\n");
				return;
			}

			if (!isValidCertFP(args[1]))
			{
				reply(x, to, c, args[1] + " is an invalid certfp.");
				return;
			}

			try
			{
				PreparedStatement stmt = Acidictive.acidcore_sql.prepare("UPDATE `access` SET `certfp` = ? WHERE `user` = ?");
				stmt.setString(1, args[1]);
				stmt.setString(2, x.getIdentNick());
				Acidictive.acidcore_sql.executeThread(stmt);
			}
			catch (SQLException e)
			{
				log.warn(null, e);
			}
			reply(x, to, c, "Updated your certfp to \"" + args[1] + "\"");
		}

		// add
		else if (args[0].equalsIgnoreCase("add"))
		{
			if (args.length < 3)
			{
				reply(x, to, c, "Invalid arguments: access add <user> <flag> [certfp]");
				return;
			}

			if (args.length > 3 && !isValidCertFP(args[3]))
			{
				reply(x, to, c, args[3] + " is an invalid certfp.");
				return;
			}

			boolean addedcertfp = (args.length > 3);
			User u = User.findUser(args[1]);
			String target;
			if (u != null && !u.getSU().isEmpty())
				target = u.getSU();
			else
				target = args[1];

			try
			{
				PreparedStatement stmt = Acidictive.acidcore_sql.prepare("SELECT * FROM `access` WHERE `user` = ?");
				stmt.setString(1, target);
				ResultSet rs = Acidictive.acidcore_sql.executeQuery(stmt);

				if (rs.next())
				{
					reply(x, to, c, "The user \"" + args[1] + "\" already exists"
							+ (target.equals(args[1]) ? " (as " + rs.getString("user") + ")" : "") + ".");
					return;
				}

				if (!x.hasFlags("SRA"))
				{
					reply(x, to, c, "You do not have access to assign +" + args[3] + " access.");
					return;
				}

				stmt = Acidictive.acidcore_sql.prepare("INSERT INTO `access`(`user`, `flags`, `certfp`) VALUES (?, ?, ?)");
				stmt.setString(1, target);
				stmt.setString(2, args[2]);
				stmt.setString(3, addedcertfp ? args[3] : "");
				Acidictive.acidcore_sql.executeThread(stmt);
			}
			catch (SQLException e)
			{
				log.warn(null, e);
				return;
			}

			if (u != null)
				addedcertfp = addedcertfp || addCertFP(u, false);

			updateFlags(args[1], args[2]);
			reply(x, to, c, "Added \"" + target + "\" with flags "
					+ args[2] + (addedcertfp ? " and certfp " + u.getCertFP() : ""));
		}

		// del
		else if (args[0].equalsIgnoreCase("del"))
		{
			try
			{
				PreparedStatement stmt = Acidictive.acidcore_sql.prepare("SELECT `flags` FROM `access` WHERE `user` = ?");
				stmt.setString(1, args[1]);
				ResultSet rs = Acidictive.acidcore_sql.executeQuery(stmt);

				if (!rs.next())
				{
					reply(x, to, c, "\"" + args[1] + "\" is not on the access list.");
					return;
				}

				String flags = rs.getString("flags");

				/* Yes, SRAs somehow can't be removed at all. Brilliant, eh?
				 * Someone slap whoever came up with +1 for me, thanks.
				 */
				if (isSRA(flags))
				{
					reply(x, to, c, "You cannot remove \"" + args[1]
							+ "\" because you are not higher than them.");
					return;
				}

				stmt = Acidictive.acidcore_sql.prepare("DELETE FROM `access` WHERE `user` = ?");
				stmt.setString(1, args[1]);
				Acidictive.acidcore_sql.executeThread(stmt);
			}
			catch (SQLException e)
			{
				log.warn(null, e);
				return;
			}

			updateFlags(args[1], "");
			reply(x, to, c, "Removed \"" + args[1] + "\" from the access list.");
		}

		// list
		else if (args[0].equalsIgnoreCase("list"))
		{
			if (!x.hasFlags("SRA"))
			{
				reply(x, to, c, "This command requires SRA");
				return;
			}

			try
			{
				PreparedStatement stmt = Acidictive.acidcore_sql.prepare("SELECT `user`,`certfp`,`flags` FROM `access` ORDER BY `user`");
				ResultSet rs = Acidictive.acidcore_sql.executeQuery(stmt);
				int i = 0;
				while (rs.next())
				{
					if ((args.length >= 2 && rs.getString("user").indexOf(args[1]) >= 0) ||
							args.length == 1)
					{
						i++;
						reply(x, to, c, i + ": " + Message.BOLD + rs.getString("user") + Message.BOLD + "/"
								+ (rs.getString("certfp").isEmpty() ? "(none)" : rs.getString("certfp")) + " - " + rs.getString("flags"));
					}
				}
				reply(x, to, c, "End of access list. [" + i + " users displayed]");
			}
			catch (SQLException e)
			{
				log.warn(null, e);
			}
		}

		// setflags
		else if (args[0].equalsIgnoreCase("setflags"))
		{
			if (args.length < 3)
			{
				reply(x, to, c, "Invalid arguments: access setflags <user> <flags>\n");
				return;
			}

			String oldflags;
			try
			{
				PreparedStatement stmt = Acidictive.acidcore_sql.prepare("SELECT * FROM `access` WHERE `user` = ?");
				stmt.setString(1, args[1]);
				ResultSet rs = Acidictive.acidcore_sql.executeQuery(stmt);
				if (!rs.next())
				{
					reply(x, to, c, "The user \"" + args[1] + "\" does not exist.");
					return;
				}

				oldflags = rs.getString("flags");

				if (isSRA(oldflags))
				{
					reply(x, to, c, "You do not have access to change the flags for a SRA user.");
					return;
				}

				stmt = Acidictive.acidcore_sql.prepare("UPDATE `access` SET `flags` = ? WHERE `user` = ?");
				stmt.setString(1, args[2]);
				stmt.setString(2, args[1]);
				Acidictive.acidcore_sql.executeThread(stmt);
			}
			catch (SQLException e)
			{
				log.warn(null, e);
				return;
			}

			updateFlags(args[1], args[2]);
			reply(x, to, c, "Updated flags for \"" + args[1] + "\" from "
						+ oldflags + " to " + args[2] + "");
		}

		// setcertfp
		else if (args[0].equalsIgnoreCase("setcertfp"))
		{
			if (args.length < 3)
			{
				reply(x, to, c, "Invalid arguments: access setcertfp <user> <certfp>\n");
				return;
			}

			if (!isValidCertFP(args[2]))
			{
				reply(x, to, c, args[2] + " is an invalid certfp.");
				return;
			}

			try
			{
				PreparedStatement stmt = Acidictive.acidcore_sql.prepare("SELECT * FROM `access` WHERE `user` = ?");
				stmt.setString(1, args[1]);
				ResultSet rs = Acidictive.acidcore_sql.executeQuery(stmt);

				if (!rs.next())
				{
					reply(x, to, c, "The user \"" + args[1] + "\" does not exist.");
					return;
				}

				String flags = rs.getString("flags"), oldcertfp = rs.getString("certfp");

				if (isSRA(flags))
				{
					reply(x, to, c, "You can not change the certfp for a SRA user.");
					return;
				}

				stmt = Acidictive.acidcore_sql.prepare("UPDATE `access` SET `certfp` = ? WHERE `user` = ?");
				stmt.setString(1, args[2]);
				stmt.setString(2, args[1]);
				Acidictive.acidcore_sql.executeThread(stmt);
				reply(x, to, c, "Updated certfp for \"" + args[1] + "\" from \""
						+ oldcertfp + "\" to \"" + args[2] + "\"");
			}
			catch (SQLException e)
			{
				log.warn(null, e);
			}
		}
	}

	@Override
	public void onHelp(User u, AcidUser to, Channel c)
	{
		Acidictive.reply(u, to, c, "\2access add <user> <flags> [certfp]\2 / Add a nick to the access list.");
		Acidictive.reply(u, to, c, "\2access del <user>\2 / Delete a user from the access list.");
		Acidictive.reply(u, to, c, "\2access changecertfp <certfp>\2 / Change your certfp.");
		Acidictive.reply(u, to, c, "\2access list\2 / List all users with access.");
		Acidictive.reply(u, to, c, "\2access setflags <user> <flags>\2 / Change the level of a user beneath you.");
		Acidictive.reply(u, to, c, "\2access setcertfp <user> <certfp>\2 / Change the certfp of another user.");
	}

	@Override
	public boolean onHelpCommand(User u, AcidUser to, Channel c)
	{
		Acidictive.reply(u, to, c, "Syntax: \2access add <user> <flags> [certfp]\2");
		Acidictive.reply(u, to, c, "        \2access del <user>\2");
		Acidictive.reply(u, to, c, "        \2access changecertfp <certfp>\2");
		Acidictive.reply(u, to, c, "        \2access list\2");
		Acidictive.reply(u, to, c, "        \2access setflags <user> <flags>\2");
		Acidictive.reply(u, to, c, "        \2access setcertfp <user> <certfp>\2");
		Acidictive.reply(u, to, c, " ");
		Acidictive.reply(u, to, c, "The access command provides means to manage people's access to the clients on " + AcidCore.me.getName() + ".");
		Acidictive.reply(u, to, c, "You should add only a user's main nick to the access list; they will be able");
		Acidictive.reply(u, to, c, "to use commands from other nicks using their certfp, which means that using");
		Acidictive.reply(u, to, c, "certfps is encouraged.");
		Acidictive.reply(u, to, c, " ");
		Acidictive.reply(u, to, c, "When adding new users to the list, make sure that you do not include the");
		Acidictive.reply(u, to, c, "leading plus sign to their flag set; flag presets would break that way.");
		Acidictive.reply(u, to, c, " ");
		Acidictive.reply(u, to, c, "SRAs cannot be removed from the access list or have their certfp/flags");
		Acidictive.reply(u, to, c, "changed; it is required to change the data directly in the database");
		Acidictive.reply(u, to, c, "in order to do so.");
		Acidictive.reply(u, to, c, " ");
		Acidictive.reply(u, to, c, "Only SRAs may use the access add and access list commands.");

		return true;
	}
}

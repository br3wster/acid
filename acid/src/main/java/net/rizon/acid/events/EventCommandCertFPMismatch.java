package net.rizon.acid.events;

import net.rizon.acid.core.User;

public class EventCommandCertFPMismatch
{
	private User u;
	private String certfp;

	public User getU()
	{
		return u;
	}

	public void setU(User u)
	{
		this.u = u;
	}

	public String getCertfp()
	{
		return certfp;
	}

	public void setCertfp(String certfp)
	{
		this.certfp = certfp;
	}
}

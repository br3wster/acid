package net.rizon.acid.events;

import net.rizon.acid.core.Channel;
import net.rizon.acid.core.User;

public class EventPart
{
	private User user;
	private Channel channel;

	public User getUser()
	{
		return user;
	}

	public void setUser(User user)
	{
		this.user = user;
	}

	public Channel getChannel()
	{
		return channel;
	}

	public void setChannel(Channel channel)
	{
		this.channel = channel;
	}
}

package net.rizon.acid.events;

import net.rizon.acid.core.User;

public class EventUserMode
{
	private User user;
	private String oldmodes;
	private String newmodes;

	public User getUser()
	{
		return user;
	}

	public void setUser(User user)
	{
		this.user = user;
	}

	public String getOldmodes()
	{
		return oldmodes;
	}

	public void setOldmodes(String oldmodes)
	{
		this.oldmodes = oldmodes;
	}

	public String getNewmodes()
	{
		return newmodes;
	}

	public void setNewmodes(String newmodes)
	{
		this.newmodes = newmodes;
	}
}
